﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ferreteria.Dao;
using Ferreteria.Modelo;

namespace Ferreteria.Vistas
{
    public partial class FrmUnidadesMedida : BaseForm
    {
        //definicion de columnas que tengra nuestro dataGridView
        string[] columns = new[] { "CODIGO", "UNIDAD MEDIDA" };
        //se almacenaran todos los registros de la base de datos en una lista
        List<UnidadesMedida> listaUnidadesMedidas = new List<UnidadesMedida>();
        //referencia a la clase DAO
        DaoUnidadesMedida daoUnidadesMedida = new DaoUnidadesMedida();

        UnidadesMedida unidades = new UnidadesMedida();

        public FrmUnidadesMedida()
        {
            InitializeComponent();
            cargarDatos();
        }

        public void cargarDatos()
        {
            try
            {
                //recupera todos los registros
                listaUnidadesMedidas = daoUnidadesMedida.getAll();

                //validacion de registros
                if (listaUnidadesMedidas != null)
                {
                    //limpiamos todos los datos para asignar la nueva informacion
                    dgvDatos.Columns.Clear();
                    //asignacion de valores al DataGridView
                    dgvDatos.DataSource = toDataTable(listaUnidadesMedidas
                        .Select(x => new { x.codigo, x.unidadMedida }).ToList(), columns);
                }
                else
                    //asignacion de valores al DataGridView solamente para encabezados
                    dgvDatos.DataSource = toDataTable(new List<UnidadesMedida>()
                        .Select(s => new { s.codigo, s.unidadMedida }).ToList(), columns);

                //diseño para que las columnas y rows se ajusten al tamaño de la informacion
                dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgvDatos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

                //Marcara toda la linea del DataGridView
                dgvDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                //posiciona el cursor en la primer celda
                dgvDatos.CurrentCell = dgvDatos.Rows[0].Cells[1];
                //columnas se ajustan al texto
                dgvDatos.AutoResizeColumns();
                //coloca el cursor sobre el dataGridView
                dgvDatos.Focus();
            }
            catch (Exception)
            {

            }
        }

        public void limpiarCampos()
        {
            txtCodigo.Text = "";
            txtUnidadMedida.Text = "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        public void setearValores()
        {
            try
            {
                unidades.unidadMedida = txtUnidadMedida.Text;
            }
            catch (Exception)
            {
                unidades = null;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            bool flag = false;
            try
            {
                setearValores();

                if (unidades.unidadMedida != null || unidades.unidadMedida != "")
                {
                    flag = daoUnidadesMedida.create(unidades);
                    limpiarCampos();

                    if (flag)
                    {
                        FrmMensaje mensaje = new FrmMensaje();
                        mensaje.ShowDialog();
                    }
                    else
                    {
                        FrmError mensaje = new FrmError();
                        mensaje.ShowDialog();
                    }
                }

                flag = false;
            }
            catch (Exception)
            {
                flag = false;
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            bool flag = false;

            try
            {
                setearValores();
                unidades.codigo = txtCodigo.Text;

                flag = daoUnidadesMedida.update(unidades);
                limpiarCampos();
                cargarDatos();

                if (flag)
                {
                    FrmMensaje mensaje = new FrmMensaje();
                    mensaje.ShowDialog();
                }
                else
                {
                    FrmError mensaje = new FrmError();
                    mensaje.ShowDialog();
                }

            }
            catch (Exception)
            {

                flag = false;
            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            try
            {
                txtCodigo.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[0].Value.ToString();
                txtUnidadMedida.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[1].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            bool flag = false;

            try
            {
                FrmAdvertencia advertencia = new FrmAdvertencia();
                if (DialogResult.OK == advertencia.ShowDialog())
                {

                    flag = daoUnidadesMedida.delete(txtCodigo.Text);
                    limpiarCampos();
                    cargarDatos();

                    if (flag)
                    {
                        FrmMensaje mensaje = new FrmMensaje();
                        mensaje.ShowDialog();
                    }
                    else
                    {
                        FrmError mensaje = new FrmError();
                        mensaje.ShowDialog();
                    }

                }
            }
            catch (Exception)
            {

            }
        }
    }
}
