﻿
namespace Ferreteria.Vistas
{
    partial class FrmUnidadesMedida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUnidadesMedida));
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.txtCodigo = new ns1.BunifuMaterialTextbox();
            this.txtUnidadMedida = new ns1.BunifuMaterialTextbox();
            this.Content.SuspendLayout();
            this.Sidebard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // Content
            // 
            this.Content.Controls.Add(this.txtUnidadMedida);
            this.Content.Controls.Add(this.txtCodigo);
            this.Content.Controls.Add(this.dgvDatos);
            this.AnimacionSidebar.SetDecoration(this.Content, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this.Content, BunifuAnimatorNS.DecorationType.None);
            this.Content.Size = new System.Drawing.Size(406, 405);
            // 
            // Sidebard
            // 
            this.Sidebard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Sidebard.BackgroundImage")));
            this.AnimacionSidebar.SetDecoration(this.Sidebard, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this.Sidebard, BunifuAnimatorNS.DecorationType.None);
            // 
            // btnCancelar
            // 
            this.AnimacionSidebarBack.SetDecoration(this.btnCancelar, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebar.SetDecoration(this.btnCancelar, BunifuAnimatorNS.DecorationType.None);
            this.btnCancelar.IconRightVisible = true;
            this.btnCancelar.IconVisible = true;
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.AnimacionSidebarBack.SetDecoration(this.btnEliminar, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebar.SetDecoration(this.btnEliminar, BunifuAnimatorNS.DecorationType.None);
            this.btnEliminar.IconRightVisible = true;
            this.btnEliminar.IconVisible = true;
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.AnimacionSidebarBack.SetDecoration(this.btnModificar, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebar.SetDecoration(this.btnModificar, BunifuAnimatorNS.DecorationType.None);
            this.btnModificar.IconRightVisible = true;
            this.btnModificar.IconVisible = true;
            this.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnGuardar
            // 
            this.AnimacionSidebarBack.SetDecoration(this.btnGuardar, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebar.SetDecoration(this.btnGuardar, BunifuAnimatorNS.DecorationType.None);
            this.btnGuardar.IconRightVisible = true;
            this.btnGuardar.IconVisible = true;
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // AnimacionSidebarBack
            // 
            this.AnimacionSidebarBack.MaxAnimationTime = 1500;
            // 
            // AnimacionSidebar
            // 
            this.AnimacionSidebar.MaxAnimationTime = 1500;
            // 
            // dgvDatos
            // 
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AnimacionSidebar.SetDecoration(this.dgvDatos, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this.dgvDatos, BunifuAnimatorNS.DecorationType.None);
            this.dgvDatos.Location = new System.Drawing.Point(25, 180);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.ReadOnly = true;
            this.dgvDatos.Size = new System.Drawing.Size(345, 177);
            this.dgvDatos.TabIndex = 0;
            this.dgvDatos.Click += new System.EventHandler(this.dgvDatos_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.AnimacionSidebar.SetDecoration(this.txtCodigo, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this.txtCodigo, BunifuAnimatorNS.DecorationType.None);
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCodigo.HintForeColor = System.Drawing.Color.Empty;
            this.txtCodigo.HintText = "CODIGO";
            this.txtCodigo.isPassword = false;
            this.txtCodigo.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtCodigo.LineIdleColor = System.Drawing.Color.Black;
            this.txtCodigo.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtCodigo.LineThickness = 3;
            this.txtCodigo.Location = new System.Drawing.Point(25, 26);
            this.txtCodigo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(208, 49);
            this.txtCodigo.TabIndex = 1;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtUnidadMedida
            // 
            this.txtUnidadMedida.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.AnimacionSidebar.SetDecoration(this.txtUnidadMedida, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this.txtUnidadMedida, BunifuAnimatorNS.DecorationType.None);
            this.txtUnidadMedida.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnidadMedida.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtUnidadMedida.HintForeColor = System.Drawing.Color.Empty;
            this.txtUnidadMedida.HintText = "UNIDAD DE MEDIDA";
            this.txtUnidadMedida.isPassword = false;
            this.txtUnidadMedida.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtUnidadMedida.LineIdleColor = System.Drawing.Color.Black;
            this.txtUnidadMedida.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtUnidadMedida.LineThickness = 3;
            this.txtUnidadMedida.Location = new System.Drawing.Point(25, 84);
            this.txtUnidadMedida.Margin = new System.Windows.Forms.Padding(4);
            this.txtUnidadMedida.Name = "txtUnidadMedida";
            this.txtUnidadMedida.Size = new System.Drawing.Size(208, 49);
            this.txtUnidadMedida.TabIndex = 2;
            this.txtUnidadMedida.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // FrmUnidadesMedida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(650, 450);
            this.AnimacionSidebar.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.AnimacionSidebarBack.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "FrmUnidadesMedida";
            this.Text = "FrmUnidadesMedida";
            this.Content.ResumeLayout(false);
            this.Sidebard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvDatos;
        private ns1.BunifuMaterialTextbox txtUnidadMedida;
        private ns1.BunifuMaterialTextbox txtCodigo;
    }
}