﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ferreteria.Vistas
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        private void MenuSidebar_Click(object sender, EventArgs e)
        {
            if (Sidebard.Width == 213) 
            {
                Sidebard.Visible = false;
                Sidebard.Width = 65;
                SidebarWrapper.Width = 90;
                AnimacionSidebar.Show(Sidebard);
            }
            else
            {
                Sidebard.Visible = false;
                Sidebard.Width = 213;
                SidebarWrapper.Width = 244;
                AnimacionSidebarBack.Show(Sidebard);
            }
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            Maximizar.Visible = false;
            Restaurar.Visible = true;
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Restaurar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Maximizar.Visible = true;
            Restaurar.Visible = false;
        }


        public static DataTable toDataTable<T>(List<T> l, string[] columns)
        {
            DataTable dt = new DataTable(typeof(T).Name);
            try
            {
                /// obtenemos todas la propiedades
                PropertyInfo[] p = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                var r = new object[p.Length];

                /// agregamos todas las columnas de la lista
                foreach (PropertyInfo pi in p)
                    dt.Columns.Add(pi.Name, pi.PropertyType);

                foreach (T o in l)
                {
                    /// se prepara cada fila a insertar
                    for (int i = 0; i < p.Length; i++)
                        r[i] = p[i].GetValue(o, null);
                    dt.Rows.Add(r);
                }

                var c = 0;
                if (dt.Columns.Count.Equals(columns.Length))
                    foreach (DataColumn i in dt.Columns)
                    {
                        i.ColumnName = columns[c];
                        c++;
                    }
            }
            catch (Exception)
            {

            }
            return dt;
        }
    }
}
