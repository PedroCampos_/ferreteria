﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ferreteria.Vistas;

namespace Ferreteria
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            Maximizar.Visible = false;
            Restaurar.Visible = true;
        }

        private void Restaurar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Maximizar.Visible = true;
            Restaurar.Visible = false;
        }

        private void MenuSidebar_Click(object sender, EventArgs e)
        {
            if (Sidebard.Width == 270)
            {
                Sidebard.Visible = false;
                Sidebard.Width = 68;
                SidebarWrapper.Width = 90;
                LineaSidebar.Width = 50;
                AnimacionSidebar.Show(Sidebard);
            }
            else
            {
                Sidebard.Visible = false;
                Sidebard.Width = 270;
                SidebarWrapper.Width = 300;
                LineaSidebar.Width = 250;
                AnimacionSidebarBack.Show(Sidebard);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmMensaje mensaje = new FrmMensaje();
            mensaje.ShowDialog();
        }
    }
}
