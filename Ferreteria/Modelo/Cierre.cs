namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cierre")]
    public partial class Cierre
    {
        [Key]
        public int idCierre { get; set; }

        public double totalVenta { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaCierre { get; set; }

        public int? idVenta { get; set; }

        public virtual Venta Venta { get; set; }
    }
}
