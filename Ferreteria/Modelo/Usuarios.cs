namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [Key]
        public int idUsuario { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [StringLength(50)]
        public string username { get; set; }

        [StringLength(50)]
        public string contrasena { get; set; }

        public int? idPersonal { get; set; }

        public virtual Personal Personal { get; set; }
    }
}
