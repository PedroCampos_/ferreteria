namespace Ferreteria.Modelo
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModeloDB : DbContext
    {
        public ModeloDB()
            : base("name=ModeloDB")
        {
        }

        public virtual DbSet<Cargo> Cargo { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<Cierre> Cierre { get; set; }
        public virtual DbSet<Compra> Compra { get; set; }
        public virtual DbSet<Distribuidora> Distribuidora { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<Marca> Marca { get; set; }
        public virtual DbSet<Permisos> Permisos { get; set; }
        public virtual DbSet<Personal> Personal { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<UnidadesMedida> UnidadesMedida { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Venta> Venta { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cargo>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Cargo>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Categoria>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Categoria>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Compra>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.imgDistribuidora)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<Distribuidora>()
                .Property(e => e.redesSociales)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.consolidado)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.redesSociales)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.imgEmpresa)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Marca>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Marca>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Marca>()
                .Property(e => e.imgMarca)
                .IsUnicode(false);

            modelBuilder.Entity<Permisos>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.dui)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.nit)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<Personal>()
                .Property(e => e.genero)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.imgProducto)
                .IsUnicode(false);

            modelBuilder.Entity<UnidadesMedida>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<UnidadesMedida>()
                .Property(e => e.unidadMedida)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.contrasena)
                .IsUnicode(false);

            modelBuilder.Entity<Venta>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Venta>()
                .Property(e => e.fechaCierre)
                .IsUnicode(false);
        }
    }
}
