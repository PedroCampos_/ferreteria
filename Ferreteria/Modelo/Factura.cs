namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        [Key]
        public int idFactura { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaFacturacion { get; set; }

        public int? idPersonal { get; set; }

        public int? idVenta { get; set; }

        public int? idEmpresa { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual Personal Personal { get; set; }

        public virtual Venta Venta { get; set; }
    }
}
