namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Distribuidora")]
    public partial class Distribuidora
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Distribuidora()
        {
            Compra = new HashSet<Compra>();
            Marca = new HashSet<Marca>();
        }

        [Key]
        public int idDistribuidora { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [Required]
        [StringLength(150)]
        public string nombre { get; set; }

        [StringLength(200)]
        public string descripcion { get; set; }

        [StringLength(200)]
        public string direccion { get; set; }

        public string imgDistribuidora { get; set; }

        [StringLength(20)]
        public string telefono { get; set; }

        [StringLength(100)]
        public string correo { get; set; }

        public string redesSociales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Compra> Compra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Marca> Marca { get; set; }
    }
}
