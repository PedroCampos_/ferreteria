namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Producto")]
    public partial class Producto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Producto()
        {
            Compra = new HashSet<Compra>();
            Venta = new HashSet<Venta>();
        }

        [Key]
        public int idProducto { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [Required]
        [StringLength(150)]
        public string nombre { get; set; }

        public string descripcion { get; set; }

        public int stock { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaVencimiento { get; set; }

        public double precioUnitario { get; set; }

        public double precioVenta { get; set; }

        [Required]
        public string imgProducto { get; set; }

        public int? idMarca { get; set; }

        public int? idUnidadMedida { get; set; }

        public int? idCategoria { get; set; }

        public virtual Categoria Categoria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Compra> Compra { get; set; }

        public virtual Marca Marca { get; set; }

        public virtual UnidadesMedida UnidadesMedida { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Venta> Venta { get; set; }
    }
}
