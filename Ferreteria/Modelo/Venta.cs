namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Venta")]
    public partial class Venta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Venta()
        {
            Cierre = new HashSet<Cierre>();
            Factura = new HashSet<Factura>();
        }

        [Key]
        public int idVenta { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaVenta { get; set; }

        public int cantidadProductos { get; set; }

        public double totalImpuestos { get; set; }

        public double subTotal { get; set; }

        public double precioTotal { get; set; }

        [StringLength(50)]
        public string fechaCierre { get; set; }

        public int? idProducto { get; set; }

        public int? idPersonal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cierre> Cierre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factura> Factura { get; set; }

        public virtual Personal Personal { get; set; }

        public virtual Producto Producto { get; set; }
    }
}
