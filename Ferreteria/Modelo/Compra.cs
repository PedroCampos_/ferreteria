namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Compra")]
    public partial class Compra
    {
        [Key]
        public int idCompra { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(6)]
        public string codigo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha { get; set; }

        public double? precioCompra { get; set; }

        public double? cantidad { get; set; }

        public int? idDistribuidora { get; set; }

        public int? idMarca { get; set; }

        public int? idProducto { get; set; }

        public int? idUnidadMedida { get; set; }

        public virtual Distribuidora Distribuidora { get; set; }

        public virtual Marca Marca { get; set; }

        public virtual Producto Producto { get; set; }

        public virtual UnidadesMedida UnidadesMedida { get; set; }
    }
}
