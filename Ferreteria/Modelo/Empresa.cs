namespace Ferreteria.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Empresa()
        {
            Factura = new HashSet<Factura>();
        }

        [Key]
        public int idEmpresa { get; set; }

        [Required]
        [StringLength(150)]
        public string nombre { get; set; }

        [Required]
        [StringLength(150)]
        public string consolidado { get; set; }

        public int? incioCorrelativo { get; set; }

        public int? finCorrelativo { get; set; }

        [StringLength(150)]
        public string correo { get; set; }

        [Required]
        [StringLength(25)]
        public string telefono { get; set; }

        public string redesSociales { get; set; }

        public string imgEmpresa { get; set; }

        [StringLength(200)]
        public string direccion { get; set; }

        public string descripcion { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaCierre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factura> Factura { get; set; }
    }
}
