﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ferreteria.Modelo;

namespace Ferreteria.Dao
{
    public class DaoUnidadesMedida
    {
        //Referencia a ModeloDB
        ModeloDB modeloDB = new ModeloDB();

        public List<UnidadesMedida> getAll()
        {
            //creacion de la lista que sera retornada
            List<UnidadesMedida> unidadesMedidas = new List<UnidadesMedida>();

            try
            {
                //recupera todos los regristros de la base de datos
                unidadesMedidas = modeloDB.UnidadesMedida.ToList();

                //validacion si encontro registros en la base de datos
                if (unidadesMedidas.Count == 0)
                    return null;
            }
            catch (Exception)
            {
                return null;
            }

            return unidadesMedidas;
        }

        public bool create(UnidadesMedida unidadesMedida)
        {
            try
            {
                //crea una referencia a la tabla para agregar el nuevo registro
                modeloDB.UnidadesMedida.Add(unidadesMedida);
                //almacena el nuevo registro en la base de datos
                modeloDB.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool update(UnidadesMedida unidadesMedida)
        {
            //creacion de referencia para recuperar el registro a modificar
            UnidadesMedida modificar = new UnidadesMedida();
            try
            {
                //recuperacion del registro de la base de datos, utilizando una expresion Lambda
                modificar = modeloDB.UnidadesMedida.FirstOrDefault(s => s.codigo.Equals(unidadesMedida.codigo));

                //validaciones para evitar errores al momento de asignar valores
                if (modificar != null || modificar.codigo != null || modificar.codigo != "")
                {
                    //asignacion de nuevos valores
                    modificar.Producto = unidadesMedida.Producto;
                    modificar.unidadMedida = unidadesMedida.unidadMedida;

                    //guardar los nuevos cambios al registro
                    modeloDB.SaveChanges();
                }
                else return false;

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool delete(string idUnidadesMedida)
        {
            //creacion de referencia para recuperar el registro a eliminar
            UnidadesMedida eliminar = new UnidadesMedida();
            try
            {
                //recuperacion del registro de la base de datos, utilizando una expresion Lambda
                eliminar = modeloDB.UnidadesMedida.FirstOrDefault(s => s.codigo.Equals(idUnidadesMedida));

                //validaciones para evitar errores al momento de asignar valores
                if (eliminar != null || eliminar.codigo != null || eliminar.codigo != "")
                {
                    //eliminar el registro de la base de datos
                    modeloDB.UnidadesMedida.Remove(eliminar);

                    //guardar los nuevos cambios en la base de datos
                    modeloDB.SaveChanges();
                }
                else return false;
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

    }
}
